DUPControl
==========

Simple control software for DUP/DLP 3Dprinter.

Notice
------
| This software is poorly written, so it may cause severe mental pain or make your eyes bleed if you look at its code.
| You've been warned.

Reuirements
-----------
 - python3
 - PyQt5
 - pip

Install ``pipenv``:

Ubuntu 17.10::

    $ sudo apt install software-properties-common python-software-properties
    $ sudo add-apt-repository ppa:pypa/ppa
    $ sudo apt update
    $ sudo apt install pipenv

Archlinux::

    $ sudo pacman -Sy python-pipenv

Distribution independent::

    $ sudo pip install pipenv

You probably need to add your user to the following groups: ``uucp``, ``dialout``, ``tty``::

    $ sudo usermod -a -G uucp *username*
    $ sudo usermod -a -G tty *username*
    $ sudo usermod -a -G dialout *username*

Installation
------------

::

    $ git clone https://bitbucket.org/m0nochr0me/dupcontrol.git
    $ cd dupcontrol
    $ pipenv install pyqt5 ewmh pyserial pillow natsort

Usage
-----

::

    $ chmod +x DUPControl.sh
    $ DUPControl.sh

Job files can be prepared with `SLAcer.js <https://lautr3k.github.io/SLAcer.js/>`_
