# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'layerwindow.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_wLayer(object):
    def setupUi(self, wLayer):
        wLayer.setObjectName("wLayer")
        wLayer.resize(640, 480)
        wLayer.setCursor(QtGui.QCursor(QtCore.Qt.BlankCursor))
        wLayer.setWindowTitle("Layer Window")
        wLayer.setToolTip("")
        wLayer.setWhatsThis("")
        wLayer.setStyleSheet("background-color: #000;")
        wLayer.setLocale(QtCore.QLocale(QtCore.QLocale.Russian, QtCore.QLocale.Russia))
        self.layerView = QtWidgets.QLabel(wLayer)
        self.layerView.setGeometry(QtCore.QRect(0, 0, 500, 250))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.layerView.sizePolicy().hasHeightForWidth())
        self.layerView.setSizePolicy(sizePolicy)
        self.layerView.setStyleSheet("background-color: #000;")
        self.layerView.setText("")
        self.layerView.setScaledContents(False)
        self.layerView.setObjectName("layerView")

        self.retranslateUi(wLayer)
        QtCore.QMetaObject.connectSlotsByName(wLayer)

    def retranslateUi(self, wLayer):
        pass


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    wLayer = QtWidgets.QWidget()
    ui = Ui_wLayer()
    ui.setupUi(wLayer)
    wLayer.show()
    sys.exit(app.exec_())

