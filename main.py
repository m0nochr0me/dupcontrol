#!/usr/bin/env python3
#
# Authors: Viktor Tereshchenko <m0nochr0mex@gmail.com>
# Direct UV Printer Controller
# Copyright (c) 2017 Viktor Tereshchenko
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:  The above copyright
# notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QFileDialog, QShortcut
from PyQt5.QtGui import QImage, QPainter, QPalette, QPixmap, QTextCursor, QKeySequence
from PyQt5.QtCore import Qt, QObject, pyqtSignal, pyqtSlot
from PyQt5.QtX11Extras import QX11Info

from controlwindow import Ui_wControl
from layerwindow import Ui_wLayer

from queue import Queue, Empty

from ewmh import EWMH

from PIL import Image, ImageQt, ImageStat, ImageFilter, ImageChops

import serial
import serial.tools.list_ports

from io import StringIO
import time
import os
import zipfile
import tempfile
import re
import natsort
import json
import threading

import settings as CONFIG

WELCOME = """
CTRL+R - Refresh connection options<br />
CTRL+O - Open job<br />
CTRL+SHIFT+S - Emergency stop<hr />
"""

class ControlWindow(QWidget, Ui_wControl):
    consUpd = pyqtSignal(str)
    progUpd = pyqtSignal(float)
    zUpd = pyqtSignal(float)
    layerChg = pyqtSignal()

    def __init__(self):
        super(ControlWindow, self).__init__()
        self.setupUi(self)

        self.stateText = ("Offline", "Online", "Error", "Complete", "Printing")

        self.state = 0

        self.job = ""
        self.jobDir = None
        self.jobLayers = []
        self.jobLayersCount = 0
        self.jobConf = None
        self.currentLayerNum = 1
        self.currentLayer = None
        self.currentLayerMean = 0
        self.layerMask = None
        self.port = None
        self.portReaderThread = None
        self.portReaderAlive = None
        self.portSenderAlive = None
        self.portSenderThread = None
        self.workThread = None
        self.workThreadAlive = None
        self.cmdQueue = Queue(0)
        self.progress = 0
        self.z = 0.0
        self.zMoveComp = False

        self.lwnd = None

        self.portReady = False

        self.prepareControls()

    @pyqtSlot(str)
    def updateTerminal(self, text):
        self.terminalLog.insertHtml(text + '<br />')
        self.terminalLog.moveCursor(QTextCursor.End)
        # pass

    def refreshConnectionOptions(self):
        self.portSelect.clear()
        for port in serial.tools.list_ports.comports():
            self.portSelect.addItem(port.device)
        self.monitorSelect.clear()
        for screen in app.screens():
            self.monitorSelect.addItem(screen.name())

    @pyqtSlot(float)
    def updateProgress(self, progress):
        self.progress = progress
        self.progressBar.setValue(progress)

    @pyqtSlot(float)
    def updateZ(self, z):
        self.z = z
        self.zPos.display(self.z)

    def zHome(self):
        if self.state == 0:
            cmd = "G28 Z0"
            self.sendCMD(cmd)
            self.updateZ(0)

    def zMove(self, z):
        if 0 <= self.z + z <= CONFIG.ZMAX and self.state == 0:
            z = round(self.z + z, 3)
            cmd = "G1 Z{} F{} P1".format(z, CONFIG.ZJOGSPEED)
            self.sendCMD(cmd)
            self.updateZ(z)

    @pyqtSlot()
    def updateLayer(self):
        jobFile = self.jobLayers[self.currentLayerNum - 1]
        print(jobFile)
        pilImg = Image.open(jobFile)
        if CONFIG.USETIMECORR:
            stat = ImageStat.Stat(pilImg)
            self.currentLayerMean = stat.mean[0]
            # print('Average layer brightness: ', self.currentLayerMean)
            self.consUpd.emit('Average layer brightness: {}'.format(self.currentLayerMean))
        if CONFIG.USEMASK:
            pilImg = ImageChops.multiply(pilImg, self.layerMask)
        img = ImageQt.ImageQt(pilImg)
        previewHeight = self.layerPreview.geometry().height()
        self.layerPreview.setPixmap(
            QPixmap.fromImage(img).scaledToHeight(previewHeight))
        if self.lwnd:
            self.lwnd.layerView.setPixmap(QPixmap.fromImage(img))

    def openJob(self):
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getOpenFileName(
            self, "QFileDialog.getOpenFileName()", "",
            "Zipped job file (*.stl.zip)", options=options)
        if fileName:
            jobFile = zipfile.ZipFile(fileName, 'r')
            if self.jobDir:
                self.jobDir.cleanup()
            self.jobDir = tempfile.TemporaryDirectory(prefix="DUPControl-")
            jobFile.extractall(self.jobDir.name)
            jobFile.close()
            sliceDir = self.jobDir.name+'/slices/'
            if not os.path.isdir(sliceDir):
                self.jobDir.cleanup()
                self.consUpd.emit('Invalid job format')
                return
            with open(self.jobDir.name + "/slacer.json") as jobConfFile:
                self.jobConf = json.load(jobConfFile)
            self.jobLayers = [sliceDir + l for l in os.listdir(sliceDir) if os.path.isfile(os.path.join(sliceDir, l))]
            self.jobLayersCount = len(self.jobLayers)
            self.jobLayers = natsort.natsorted(self.jobLayers)
            if CONFIG.USEMASK:
                self.layerMask = Image.open('mask.png')
            self.layerChg.emit()
            eta = (self.jobConf['liftingHeight']*2 - self.jobConf['layersHeight'])/(self.jobConf['liftingSpeed']/60)
            eta = (eta + self.jobConf['exposureTime']/1000)*self.jobLayersCount
            text = "Job '{}' loaded. ({})<br />Layers: {}<br />{}mm/{}ms<br />ETA: {}".format(
                fileName,
                time.ctime(),
                self.jobLayersCount,
                self.jobConf['layersHeight'],
                self.jobConf['exposureTime'],
                time.strftime("%H:%M:%S", time.gmtime(eta*1.1)))
            self.consUpd.emit(text)

    def closeEvent(self, event):
        if self.jobDir:
            self.jobDir.cleanup()
        self.portDisconnect()
        sys.exit(0)

    def sendCMD(self, commands):
        if self.port and commands:
            buf = StringIO(commands)
            for command in buf:
                if command.find(';') > -1:
                    command = command[:command.index(';')]
                command = command.strip()
                if re.search("(G1|G28)", command):
                    self.zMoveComp = False
                if command:
                    self.cmdQueue.put(command)
                    while not self.zMoveComp and self.workThreadAlive:
                        time.sleep(0.001)

    def _portSendCMD(self, command):
        if self.port and command:
            while not self.portReady and self.portSenderAlive:
                time.sleep(0.001)
            self.portReady = False
            print('> ' + command)
            self.consUpd.emit(command)
            self.port.write(command.encode() + b'\n\r')
            while not self.portReady and self.portSenderAlive:
                time.sleep(0.001)

    def portSender(self):
        while self.port and self.portSenderAlive:
            try:
                data = self.cmdQueue.get(True, 0.1)
            except Empty:
                continue
            self._portSendCMD(data)

    def portReader(self):
        self.portReady = True
        try:
            while self.port and self.portReaderAlive:
                text = ""
                data = self.port.readline()
                if data:
                    try:
                        text = data.decode('utf-8')
                        text = text.strip()
                    except UnicodeDecodeError:
                        text = "@"
                    if text:
                        print('< ' + text)
                        self.consUpd.emit('<i>' + text + '</i>')
                    if text.startswith('Z_move_comp'):
                        self.zMoveComp = True
                    if text.startswith('ok'):
                        self.portReady = True
        except serial.SerialException:
            self.portReaderAlive = False
            raise
        self.portReady = True

    def portDisconnect(self):
        if self.port:
            self.portReaderAlive = False
            self.portSenderAlive = False
            self.workThreadAlive = False
            self.port.cancel_read()
            self.portReaderThread.join()
            self.portSenderThread.join()
            if self.workThread:
                self.workThread.join()
            self.port.close()
            self.port = None
            self.updateState(0)
            self.btnConnect.setText('Connect')
            text = 'Machine disconnected.'
            self.consUpd.emit(text)
            self.updateState(0)
        if self.lwnd:
            self.lwnd.close()

    def portConnect(self):
        if not self.port:
            port = self.portSelect.currentText()
            baud = self.baudSelect.currentText()
            try:
                self.port = serial.Serial(
                    port, baud, timeout=0.25,
                    parity=serial.PARITY_NONE, dsrdtr=True)
            except serial.SerialException:
                text = 'Connection failed!'
                self.consUpd.emit(text)
                self.btnConnect.setChecked(False)
                return
            self.port.write(b'\n\r\n\r')
            self.port.flushInput()
            self.port.dtr = True
            time.sleep(0.2)
            self.port.dtr = False
            self.portReaderAlive = True
            self.portReaderThread = threading.Thread(
                target=self.portReader, name='rx')
            self.portReaderThread.daemon = True
            self.portReaderThread.start()
            self.portSenderAlive = True
            self.portSenderThread = threading.Thread(
                target=self.portSender, name='tx')
            self.portSenderThread.daemon = True
            self.portSenderThread.start()
            self.zMoveComp = True
            text = 'Connection established.'
            self.consUpd.emit(text)

            self.btnConnect.setText('Disconnect')

            self.showLayerWindow()
            self.updateState(1)
        else:
            self.portDisconnect()

    def showLayerWindow(self):
        if not self.lwnd:
            self.lwnd = LayerWindow()
        self.lwnd.show()
        screenIndex = self.monitorSelect.currentIndex()
        self.lwnd.moveToScreen(screenIndex)
        self.lwnd.showFullScreen()
        self.lwnd.showOnAllWorkspaces()

    def emergencyStop(self):
        if self.port:
            self.updateState(2)
            # cmd = "M112"
            # self.sendCMD(cmd)
            self.port.dtr = True
            time.sleep(0.2)
            self.port.dtr = False
            text = '<span style=" color:#960e0e;">Emergency Stop! ({})</span>'.format(time.ctime())
            self.consUpd.emit(text)
        self.portDisconnect()
        self.btnConnect.setChecked(False)
        # self.portConnect()

    def processJob(self):
        if self.port:

            self.consUpd.emit('Print START ({})'.format(time.ctime()))

            self.sendCMD(CONFIG.GCODE_PREFIX)

            for layer in self.jobLayers:
                if not self.workThreadAlive:
                    print('Work aborted')
                    return
                self.layerChg.emit()
                self.progUpd.emit(self.currentLayerNum*100/self.jobLayersCount)
                self.zUpd.emit(
                    self.currentLayerNum*self.jobConf['layersHeight'])
                self.consUpd.emit("LAYER: {}".format(self.currentLayerNum))
                if self.currentLayerNum > 1:
                    self.sendCMD(
                        "G1 Z{} F{} P1".format(self.jobConf['liftingHeight'], self.jobConf['liftingSpeed']))
                    self.sendCMD(
                        "G1 Z-{} F{} P1".format(self.jobConf['liftingHeight'] - self.jobConf['layersHeight'], self.jobConf['liftingSpeed']))

                if self.currentLayerNum == 1:
                    t = (self.jobConf['exposureTime']/1000*6)
                elif self.z < 0.35:
                    t = (self.jobConf['exposureTime']/1000*3)
                elif self.z < 2:
                    t = (self.jobConf['exposureTime']/1000*2.5)
                else:
                    t = (0.225*self.currentLayerMean + self.jobConf['exposureTime']/1000)
                    if t > (self.jobConf['exposureTime']/1000)*1.2:
                        t = (self.jobConf['exposureTime']/1000)*1.2

                print('Exposure time: ', t)
                print('UV ON')
                self.sendCMD(CONFIG.GCODE_UV_ON)

                time.sleep(t)

                self.sendCMD(CONFIG.GCODE_UV_OFF)
                print('UV OFF')

                self.currentLayerNum = self.currentLayerNum + 1

            self.sendCMD(CONFIG.GCODE_POSTFIX)

            self.consUpd.emit('Print DONE ({})'.format(time.ctime()))

            self.updateState(3)

    def startPrint(self):
        if self.state == 3:
            self.consUpd.emit('It is not safe to start printing.<br />' +
                'Ensure build plate is clear, then reconnect.')
        if self.state == 1 and self.jobLayers and self.port:
            self.updateState(4)
            self.currentLayerNum = 1
            self.workThreadAlive = True
            self.workThread = threading.Thread(
                target=self.processJob, name="work")
            self.workThread.daemon = True
            self.workThread.start()

    def updateState(self, state):
        self.state = state
        self.labelState.setText(self.stateText[state])

    def prepareControls(self):
        # Setup and connect components

        self.refreshConnectionOptions()

        self.updateZ(0)

        self.updateProgress(0)

        self.btnZHome.clicked.connect(self.zHome)
        self.btnZ01UP.clicked.connect(lambda: self.zMove(0.1))
        self.btnZ01DN.clicked.connect(lambda: self.zMove(-0.1))
        self.btnZ1UP.clicked.connect(lambda: self.zMove(1))
        self.btnZ1DN.clicked.connect(lambda: self.zMove(-1))
        self.btnZ10UP.clicked.connect(lambda: self.zMove(10))
        self.btnZ10DN.clicked.connect(lambda: self.zMove(-10))
        self.btnZ50UP.clicked.connect(lambda: self.zMove(50))
        self.btnZ50DN.clicked.connect(lambda: self.zMove(-50))
        self.btnOpen.clicked.connect(self.openJob)
        self.btnConnect.clicked.connect(self.portConnect)
        self.portRefreshShortcut = QShortcut(QKeySequence("Ctrl+R"), self)
        self.portRefreshShortcut.activated.connect(self.refreshConnectionOptions)
        self.btnSend.clicked.connect(lambda: self.sendCMD(self.command.text()))
        self.command.returnPressed.connect(lambda: self.sendCMD(self.command.text()))
        self.btnEmergencyStop.clicked.connect(self.emergencyStop)
        self.btnStart.clicked.connect(self.startPrint)

        # signals
        self.consUpd.connect(self.updateTerminal)
        self.zUpd.connect(self.updateZ)
        self.progUpd.connect(self.updateProgress)
        self.layerChg.connect(self.updateLayer)

        text = ('Welcome to DUPControl({})<hr />'.format(time.ctime()) +
            WELCOME)
        self.consUpd.emit(text)


class LayerWindow(QWidget, Ui_wLayer):
    def __init__(self):
        super(LayerWindow, self).__init__()
        self.setupUi(self)
        self.ewmh = EWMH()
        self.ewin = self.ewmh._createWindow(int(self.winId()))

    def resizeEvent(self, event):
        h = event.size().height()
        w = event.size().width()
        self.layerView.resize(w, h)

    def moveToScreen(self, screenIndex):
        screen = app.screens()[screenIndex]
        self.windowHandle().setScreen(screen)
        self.move(screen.geometry().x(), screen.geometry().y())
        self.resize(screen.geometry().width(), screen.geometry().height())
        # self.windowHandle().setVisibility(1)

    def showOnAllWorkspaces(self):
        self.ewmh.setWmState(self.ewin, 1, '_NET_WM_STATE_STICKY')
        self.ewmh.display.flush()


if __name__ == '__main__':

    app = QApplication(sys.argv)

    cwnd = ControlWindow()
    cwnd.show()

    sys.exit(app.exec_())
